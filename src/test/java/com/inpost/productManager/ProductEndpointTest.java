package com.inpost.productManager;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.math.BigDecimal;
import java.util.Date;
import java.util.UUID;

import javax.persistence.EntityManager;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import com.inpost.productManager.domain.Product;
import com.inpost.productManager.service.ProductService;
import com.inpost.productManager.service.ProductServiceAbstract;
/**
 * @author Radoslaw Baltrukiewicz
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
@Transactional
public class ProductEndpointTest extends BaseControllerTest {

	@Autowired
	private EntityManager entityManager;
	
	@Autowired
	private ProductService productService;
	
	private Product testProduct;
	
    @Before
    public void setup() throws Exception {
    	super.setup();

    	// create test products
    	productService.save(createProduct(100L,new BigDecimal(11.5)));
    	productService.save(createProduct(1000L,new BigDecimal(30)));
    	productService.save(createProduct(500L,new BigDecimal(20)));
    	@SuppressWarnings("deprecation")
		Page<Product> productsPage = productService.findAll(new PageRequest(0, ProductServiceAbstract.DEFAULT_PAGE_SIZE));
		assertNotNull(productsPage);
		assertEquals(3L, productsPage.getTotalElements());
		testProduct = productsPage.getContent().get(0);
		assertNotNull(testProduct);
		entityManager.refresh(testProduct);
    }
    /**
     * Test get loan by ID
     */
    @Test
    public void getProductById() throws Exception {
    	Product testProduct = productService.save(createProduct(10L,new BigDecimal(11.5)));
		mockMvc.perform(get("/v1/product/{id}", testProduct.getUuid())).andDo(print()).andExpect(status().isOk())
    	.andDo(mvcResult -> {
            String json = mvcResult.getResponse().getContentAsString();
            Product returnedProduct = (Product) convertJSONStringToObject(json);
            if(!returnedProduct.getAmount().equals(new BigDecimal(11.5)))
            fail("returned amount does not match"+returnedProduct.toString());
    	});

    	
    }

    /**
     * Test create product success. 
     */
    @Test
    public void createProductReturnedSuccess() throws Exception {
    	
    	testProduct = new Product(BigDecimal.valueOf(1), BigDecimal.ONE);
		mockMvc.perform(post("/v1/product").contentType(JSON_MEDIA_TYPE).content(json(testProduct))).andDo(print())
		.andExpect(status().isOk())
    	.andDo(mvcResult -> {
            String json = mvcResult.getResponse().getContentAsString();
            Product returnedProduct = (Product) convertJSONStringToObject(json);
            if(!returnedProduct.getAmount().equals(new BigDecimal(1)))
            fail("returned amount does not match"+returnedProduct.toString());
    	});

    }
    /**
     * Test not supported method
     */
    @Test
    public void handleHttpRequestMethodNotSupportedException() throws Exception {
    	
    	String content = json(testProduct);
    	
    	mockMvc.perform(
    			delete("/v1/product")
    			.header(ProductServiceAbstract.HEADER_PRODUCT_UUID, UUID.randomUUID())
    			.accept(JSON_MEDIA_TYPE)
    			.content(content)
    			.contentType(JSON_MEDIA_TYPE))
    	.andDo(print())
    	.andExpect(status().isMethodNotAllowed())
    	.andExpect(content().string(""))
    	;
    }

	private Product createProduct(Long price, BigDecimal amount) {
		Product product = new Product(BigDecimal.valueOf(price),amount);
		product.setCreatedDate(new Date());
		return product;
	}

}
