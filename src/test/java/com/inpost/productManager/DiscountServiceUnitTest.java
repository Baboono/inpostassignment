package com.inpost.productManager;

import static org.junit.Assert.assertEquals;

import java.math.BigDecimal;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.inpost.productManager.domain.Discount;
import com.inpost.productManager.domain.Product;
import com.inpost.productManager.enums.DiscountPolicy;
import com.inpost.productManager.service.DiscountService;
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
public class DiscountServiceUnitTest {
	
	@Autowired
	DiscountService discountService;
	
	@Test
	public void calculateDiscountedPrice1() throws Exception {
		Discount discount1 = createDiscount(BigDecimal.valueOf(Double.valueOf(0.1)), BigDecimal.valueOf(Double.valueOf(0)), DiscountPolicy.PERCENT);
		Product product1 = new Product(BigDecimal.valueOf(1.5), BigDecimal.valueOf(2));
		BigDecimal result = discountService.calculateDiscountedPrice(product1, discount1).setScale(1);
		assertEquals(BigDecimal.valueOf(Double.valueOf(2.7)), result);
		
	}
	@Test
	public void calculateDiscountedPrice2() throws Exception {
		Discount discount1 = createDiscount(BigDecimal.valueOf(Double.valueOf(0.5)), BigDecimal.valueOf(Double.valueOf(99999)), DiscountPolicy.PERCENT);
		Product product1 = new Product(BigDecimal.valueOf(2), BigDecimal.valueOf(50));
		BigDecimal result = discountService.calculateDiscountedPrice(product1, discount1).setScale(1);
		assertEquals(BigDecimal.valueOf(Double.valueOf(50)), result);
		
	}
	@Test
	public void calculateDiscountedPrice3() throws Exception {
		Discount discount1 = createDiscount(BigDecimal.valueOf(Double.valueOf(0.1)), BigDecimal.valueOf(Double.valueOf(51)), DiscountPolicy.QUANTITY);
		Product product1 = new Product(BigDecimal.valueOf(2), BigDecimal.valueOf(50));
		BigDecimal result = discountService.calculateDiscountedPrice(product1, discount1).setScale(1);
		assertEquals(BigDecimal.valueOf(Double.valueOf(100)), result);
		
	}
	@Test
	public void calculateDiscountedPrice4() throws Exception {
		Discount discount1 = createDiscount(BigDecimal.valueOf(Double.valueOf(0.1)), BigDecimal.valueOf(Double.valueOf(49)), DiscountPolicy.QUANTITY);
		Product product1 = new Product(BigDecimal.valueOf(2), BigDecimal.valueOf(50));
		BigDecimal result = discountService.calculateDiscountedPrice(product1, discount1).setScale(1);
		assertEquals(BigDecimal.valueOf(Double.valueOf(50)), result);
		
	}
	private Discount createDiscount(BigDecimal amount,BigDecimal minAmount, DiscountPolicy discountPolicy) {
		Discount discount = new Discount(amount,discountPolicy);
		discount.setMinAmount(minAmount);
		return discount;
	}
}
