package com.inpost.productManager;

import static org.hamcrest.Matchers.is;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.math.BigDecimal;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import com.inpost.productManager.domain.Product;
import com.inpost.productManager.service.ProductService;

/**
 * Main endpoint mocked tests
 * 
 * @author Radoslaw Baltrukiewicz
 * 
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
@Transactional
public class ProductControllerMockedTest extends BaseControllerTest {

	@MockBean
	private ProductService productService;

	private Product testProduct;

	@Before
	public void setup() throws Exception {
		super.setup();
		testProduct = new Product(BigDecimal.valueOf(1.5), BigDecimal.ONE);
		when(productService.findOne(anyString())).thenReturn(testProduct);
		when(productService.save(testProduct)).thenReturn(testProduct);
	}

	@Test
	public void getProductById() throws Exception {

		mockMvc.perform(get("/v1/product/{id}", 1)).andDo(print()).andExpect(status().isOk())
				.andExpect(content().contentType(JSON_MEDIA_TYPE)).andExpect(jsonPath("$.price", is(1.5)))
				.andExpect(jsonPath("$.amount", is(1)));
	}
}
