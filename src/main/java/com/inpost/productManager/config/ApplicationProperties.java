package com.inpost.productManager.config;
import java.math.BigDecimal;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

/**
 * Configuration. Binded to application.properties file
 * 
 * @author Radoslaw Baltrukiewicz
 * 
 */
@Component
@PropertySource("classpath:application.properties")
public class ApplicationProperties {
	//DISCOUNT AMOUNTS

	@Value("${productManagerDemo.config.discount.discountQuantitive.amount}")
    private String discountQuantitiveAmount;
	
	@Value("${productManagerDemo.config.discount.discountPercent.amount}")
    private String discountPercentAmount;

	public BigDecimal getDiscountQuantitiveAmount() {
		return BigDecimal.valueOf(Double.valueOf(discountQuantitiveAmount));
	}

	public BigDecimal getDiscountPercentAmount() {
		return BigDecimal.valueOf(Double.valueOf(discountPercentAmount));
	}

}
