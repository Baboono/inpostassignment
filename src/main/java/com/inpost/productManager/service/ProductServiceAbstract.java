package com.inpost.productManager.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.inpost.productManager.domain.GenericProductI;
import com.inpost.productManager.domain.Product;



public abstract class ProductServiceAbstract {
	
	public static final int DEFAULT_PAGE_SIZE = 10;
	public static final String HEADER_PRODUCT_UUID = "productId";
	abstract Page<?> findAll(Pageable pageable);
	
	abstract GenericProductI save(Product product) throws Exception;

	abstract GenericProductI findOne(String uuid) throws Exception;
	


}
