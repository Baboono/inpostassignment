package com.inpost.productManager.service;

import org.hibernate.Hibernate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.inpost.productManager.domain.Product;
import com.inpost.productManager.repository.ProductRepository;

/**
 * Service involving for all product entity based operations
 * 
 * @author Radoslaw Baltrukiewicz
 * 
 */
@Service
@Transactional
public class ProductService extends ProductServiceAbstract {

	@Autowired
	private ProductRepository repository;

	@Override
	@Transactional(readOnly = true)
	public Page<Product> findAll(Pageable pageable) {
		return repository.findAll(pageable);
	}

	@Override
	@Transactional(readOnly = true)
	public Product findOne(String uuid)  {
		try {
			Product product = repository.findByUuid(uuid);
			Hibernate.initialize(product);
			return product;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Override
	@Transactional
	public Product save(Product product) {
		try {
			if(product == null) {
				return null;
			}
			return repository.saveAndFlush(product);	 
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

}
