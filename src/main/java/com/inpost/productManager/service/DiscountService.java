package com.inpost.productManager.service;

import java.math.BigDecimal;

import org.hibernate.Hibernate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.inpost.productManager.config.ApplicationProperties;
import com.inpost.productManager.domain.Discount;
import com.inpost.productManager.domain.Product;
import com.inpost.productManager.enums.DiscountPolicy;
import com.inpost.productManager.repository.DiscountRepository;

@Service
@Transactional
public class DiscountService {

	@Autowired
	private ApplicationProperties applicationProperties;

	@Autowired
	private DiscountRepository repository;

	private static final BigDecimal DEFAULT_MIN_AMOUNT = BigDecimal.ZERO;

	@Transactional(readOnly = true)
	public Page<Discount> findAll(Pageable pageable) {
		return repository.findAll(pageable);
	}

	@Transactional(readOnly = true)
	public Discount findOne(Long id) {
		try {
			Discount discount = repository.getOne(id);
			Hibernate.initialize(discount);
			return discount;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Transactional
	public Discount save(Discount discount) {
		try {
			if (discount == null) {
				return null;
			}
			if (discount.getDiscountPolicy().equals(DiscountPolicy.PERCENT)) {
				discount.setAmount(applicationProperties.getDiscountPercentAmount());
			} else if (discount.getDiscountPolicy().equals(DiscountPolicy.QUANTITY)) {
				discount.setAmount(applicationProperties.getDiscountQuantitiveAmount());
			} else {
				discount.setAmount(BigDecimal.ZERO);
			}
			if (discount.getMinAmount() == null) {
				discount.setMinAmount(DEFAULT_MIN_AMOUNT);
			}

			return repository.saveAndFlush(discount);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	public BigDecimal calculateDiscountedPrice(Product product, Discount discount) {

		if (product == null || product.getAmount() == null || product.getPrice() == null || discount == null) {
			return BigDecimal.ZERO;
		}
		BigDecimal totalForProduct = product.getAmount().multiply(product.getPrice());
		if (discount.getDiscountPolicy().equals(DiscountPolicy.PERCENT)) {
			BigDecimal discountForProduct = totalForProduct.multiply(discount.getAmount());
			return totalForProduct.subtract(discountForProduct);
		} else if (discount.getDiscountPolicy().equals(DiscountPolicy.QUANTITY)) {
			boolean compareResult = product.getAmount().compareTo(discount.getMinAmount()) == 1;
			if (compareResult) {
				BigDecimal discountForProduct = totalForProduct
						.multiply(applicationProperties.getDiscountQuantitiveAmount());
				return totalForProduct.subtract(discountForProduct);
			} else {
				return totalForProduct;
			}

		} else {
			return totalForProduct;
		}
	}

}
