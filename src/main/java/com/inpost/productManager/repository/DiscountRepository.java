package com.inpost.productManager.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.inpost.productManager.domain.Discount;

public interface DiscountRepository extends JpaRepository<Discount, Long>  {

}
