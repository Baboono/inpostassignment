package com.inpost.productManager.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.inpost.productManager.domain.Product;
/**
 * Default spring repository
 * 
 * @author Radoslaw Baltrukiewicz
 * 
 */
@Repository
public interface ProductRepository extends JpaRepository<Product, String> {
	
	Product findByUuid(String uuid);

}