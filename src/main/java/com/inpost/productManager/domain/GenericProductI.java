package com.inpost.productManager.domain;

import java.math.BigDecimal;



public interface GenericProductI {

	void setPrice(BigDecimal price);

	BigDecimal getPrice();

	void setAmount(BigDecimal amount);

	BigDecimal getAmount();

	String getUuid();

}
