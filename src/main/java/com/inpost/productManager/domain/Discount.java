package com.inpost.productManager.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Null;

import org.springframework.lang.Nullable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;
import com.inpost.productManager.enums.DiscountPolicy;

@Entity
@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
public class Discount implements DiscountI, Serializable {

	private static final long serialVersionUID = 6339818275462665418L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "discount_id")
	private Long id;

	@JsonProperty(access = Access.READ_ONLY)
	@Column(name = "amount", nullable = false)
	private BigDecimal amount;

	@Column(name = "minAmount", nullable = false)
	private BigDecimal minAmount;

	@Column(name = "policy")
	@NotNull
	private DiscountPolicy discountPolicy;

	@Column(name = "createdDate")
	@NotNull
	private Date createdDate = new Date();

	public Long getId() {
		return id;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public DiscountPolicy getDiscountPolicy() {
		return discountPolicy;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public BigDecimal getMinAmount() {
		return minAmount;
	}

	public void setMinAmount(BigDecimal minAmount) {
		this.minAmount = minAmount;
	}

	public Discount(BigDecimal amount, DiscountPolicy discountPolicy) {
		this.discountPolicy = discountPolicy;
		this.amount = amount;
	}

	public Discount() {
	}
}
