package com.inpost.productManager.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;

/**
 * Product entity class
 * 
 * @author Radoslaw Baltrukiewicz
 * 
 */
@Entity
@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
public class Product implements GenericProductI, Serializable {

	private static final long serialVersionUID = -4957442660234486916L;

	@Id
	@JsonProperty(access = Access.READ_ONLY)
	@Column(name = "uuid")
	private String uuid = UUID.randomUUID().toString();

	@NotNull
	@Column(name = "amount", nullable = false)
	private BigDecimal amount;

	@Column(name = "price")
	@NotNull
	private BigDecimal price;

	@Column(name = "createdDate")
	private Date createdDate;

	public Product(BigDecimal price, BigDecimal amount) {
		this.price = price;
		this.amount = amount;
	}

	public Product() {
	}

	@Override
	public String getUuid() {
		return uuid;
	}

	@Override
	public BigDecimal getAmount() {
		return amount;
	}

	@Override
	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	@Override
	public BigDecimal getPrice() {
		return price;
	}

	@Override
	public void setPrice(BigDecimal cost) {
		this.price = cost;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	@Override
	public int hashCode() {
		return super.hashCode();
	}

	/**
	 * Simplified version of equals for comparing products.
	 * 
	 */
	@Override
	public boolean equals(Object obj) {

		if (obj == null) {
			return false;
		}
		if (!obj.getClass().equals(Product.class))
			return false;
		Product product = (Product) obj;
		if (getUuid() != null && product.getUuid() != null)
			return getUuid().equals(product.getUuid());
		else {
			return false;
		}

	}

}