package com.inpost.productManager.controller;

import java.math.BigDecimal;
import java.util.Locale;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.inpost.productManager.domain.Discount;
import com.inpost.productManager.domain.Product;
import com.inpost.productManager.service.DiscountService;
import com.inpost.productManager.service.ProductService;
import com.inpost.productManager.service.ProductServiceAbstract;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

/**
 * Class responsible for receiving product requests as JSON, parsing them and
 * pushing further to business logic in Service(s)
 * 
 * @author Radoslaw Baltrukiewicz
 * 
 */
@RestController
@RequestMapping(produces = { MediaType.APPLICATION_JSON_VALUE })
@Validated
public class ProductController extends BaseController {

	private static final Logger log = LoggerFactory.getLogger(ProductController.class);

	@Autowired
	ProductService productService;
	
	@Autowired
	DiscountService discountService;

	@RequestMapping(path = "/v1/products", method = RequestMethod.GET)
	@ApiOperation(value = "Get all products", notes = "Returns first N products specified by the size parameter with page offset specified by page parameter.", response = Page.class)
	public Page<Product> getAll(
			@ApiParam("The size of the page to be returned") @RequestParam(required = false) Integer size,
			@ApiParam("Zero-based page index") @RequestParam(required = false) Integer page) {

		if (size == null) {
			size = ProductServiceAbstract.DEFAULT_PAGE_SIZE;
		}
		if (page == null) {
			page = 0;
		}

		@SuppressWarnings("deprecation")
		Page<Product> products = productService.findAll(new PageRequest(page, size));
		log.info(messageSource.getMessage("products.getAllSuccess", null, Locale.getDefault()));
		return products;
	}

	@RequestMapping(path = "/v1/product/{uuid}", method = RequestMethod.GET)
	@ApiOperation(value = "Get product by uuid", notes = "Returns product for id specified.", response = Product.class)
	@ApiResponses(value = { @ApiResponse(code = 404, message = "Product not found") })
	public ResponseEntity<Object> get(@ApiParam("Product id") @PathVariable("uuid") String uuid) {
		Product product;
		try {
			product = productService.findOne(uuid);
			log.info(messageSource.getMessage("products.getOneFound", null, Locale.getDefault()));
			return (product == null ? ResponseEntity.status(HttpStatus.NOT_FOUND) : ResponseEntity.ok()).body(product);
		} catch (Exception e) {
			log.warn(messageSource.getMessage("product.notRetrieved", null, Locale.getDefault()));
			return ResponseEntity.badRequest().body("Could not retrive product with uuid: " + uuid);
		}

	}

	@RequestMapping(path = "/v1/product", method = RequestMethod.POST, consumes = { MediaType.APPLICATION_JSON_VALUE })
	@ApiOperation(value = "Creates product ", notes = "Created product. Returns created product with product UUID.", response = Product.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Succesfully created product"),
			@ApiResponse(code = 400, message = "Validation criteria is not met") })
	public ResponseEntity<Object> createProduct(
			@Valid @ApiParam(value = "The product to create", required = true) @RequestBody Product product) {
		try {
			product = productService.save(product);
			log.info(messageSource.getMessage("product.added", null, Locale.getDefault()) + " UUID :" + product.getUuid());
			return ResponseEntity.ok().body(product);
		} catch (Exception e) {
			log.warn(messageSource.getMessage("product.applicationFailed", null, Locale.getDefault()));
			return ResponseEntity.badRequest().build();
		}
	}
	@RequestMapping(path = "/v1/product/discount/price", method = RequestMethod.GET)
	@ApiOperation(value = "Calculates discount on product ", notes = "Calculates product price after discount", response = BigDecimal.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Succesfully calculated discount price"),
			@ApiResponse(code = 400, message = "Validation criteria is not met") })
	public ResponseEntity<Object> getDiscountedProductPrice(@ApiParam("UUID of the product ") @RequestParam(required = true) String productUUID,
			@ApiParam("Id of the discount") @RequestParam(required = true) Long  discountId)  {
		try {
			Discount discount = discountService.findOne(discountId);
			Product product = productService.findOne(productUUID);
			if(discount==null || product==null) {
				return ResponseEntity.badRequest().build();
			}
			BigDecimal result = discountService.calculateDiscountedPrice(product,discount);
			return ResponseEntity.ok().body(result);
		} catch (Exception e) {
			log.warn(messageSource.getMessage("product.applicationFailed", null, Locale.getDefault()));
			return ResponseEntity.badRequest().build();
		}
	}
}