package com.inpost.productManager.controller;

import javax.validation.ConstraintViolationException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.validation.BindException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.ServletRequestBindingException;
import org.springframework.web.bind.annotation.ExceptionHandler;
/**
 *Exception handler class
 * @author Radoslaw Baltrukiewicz
 * 
 */
public abstract class BaseController {

	@Autowired
	protected MessageSource messageSource;

	@ExceptionHandler
	protected ResponseEntity<?> handleBindException(BindException exception) {
		return ResponseEntity.badRequest().body(exception.getMessage());
	}

	/**
	 * Exception handler for validation errors caused by method
	 * parameters @RequesParam, @PathVariable, @RequestHeader annotated with
	 * javax.validation constraints.
	 */
	@ExceptionHandler
	protected ResponseEntity<?> handleConstraintViolationException(ConstraintViolationException exception) {
		return ResponseEntity.badRequest().body("Header parameter(s) not withing range. " + exception.getMessage());
	}

	/**
	 * Exception handler for @RequestBody product validation errors.
	 */
	@ExceptionHandler
	protected ResponseEntity<?> handleMethodArgumentNotValidException(MethodArgumentNotValidException exception) {

		return ResponseEntity.badRequest().body(exception.getMessage());
	}

	/**
	 * Exception handler for missing required parameters errors.
	 */
	@ExceptionHandler
	protected ResponseEntity<?> handleServletRequestBindingException(ServletRequestBindingException exception) {

		return ResponseEntity.badRequest().body(exception.getMessage());
	}

	/**
	 * Exception handler for invalid payload (e.g. json invalid format error).
	 */
	@ExceptionHandler
	protected ResponseEntity<?> handleHttpMessageNotReadableException(HttpMessageNotReadableException exception) {

		return ResponseEntity.badRequest().body(exception.getMessage());
	}

	@ExceptionHandler
	protected ResponseEntity<?> handleNullPointerException(NullPointerException exception) {

		return ResponseEntity.badRequest().body(exception.getMessage());
	}

}
