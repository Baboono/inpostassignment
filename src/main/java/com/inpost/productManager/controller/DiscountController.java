package com.inpost.productManager.controller;

import java.util.Locale;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.inpost.productManager.domain.Discount;
import com.inpost.productManager.service.DiscountService;
import com.inpost.productManager.service.ProductService;
import com.inpost.productManager.service.ProductServiceAbstract;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

/**
 * Class responsible for receiving product requests as JSON, parsing them and
 * pushing further to business logic in Service(s)
 * 
 * @author Radoslaw Baltrukiewicz
 * 
 */
@RestController
@RequestMapping(produces = { MediaType.APPLICATION_JSON_VALUE })
@Validated
public class DiscountController extends BaseController {

	private static final Logger log = LoggerFactory.getLogger(DiscountController.class);

	@Autowired
	ProductService productService;
	@Autowired
	DiscountService disctountService;
	
	@SuppressWarnings("deprecation")
	@RequestMapping(path = "/v1/discounts", method = RequestMethod.GET)
	@ApiOperation(value = "Get all discounts", notes = "Returns first N discounts specified by the size parameter with page offset specified by page parameter.", response = Page.class)
	public Page<Discount> getAllDiscounts(
			@ApiParam("The size of the page to be returned") @RequestParam(required = false) Integer size,
			@ApiParam("Zero-based page index") @RequestParam(required = false) Integer page) {

		if (size == null) {
			size = 10;
		}
		if (page == null) {
			page = 0;
		}
		return disctountService.findAll(new PageRequest(page, size));
	}

	@RequestMapping(path = "/v1/discount", method = RequestMethod.POST, consumes = { MediaType.APPLICATION_JSON_VALUE })
	@ApiOperation(value = "Creates discount ", notes = "Created discount. Returns created discount with id.", response = Discount.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Succesfully created discount"),
			@ApiResponse(code = 400, message = "Validation criteria is not met") })
	public ResponseEntity<Object> createDiscount(
			@Valid @ApiParam(value = "The discount to create", required = true) @RequestBody Discount discount) {
		try {
			discount = disctountService.save(discount);
			log.info(messageSource.getMessage("discount.added", null, Locale.getDefault()) + " Id:" + discount.getId());
			return ResponseEntity.ok().body(discount);
		} catch (Exception e) {
			log.warn(messageSource.getMessage("product.applicationFailed", null, Locale.getDefault()));
			return ResponseEntity.badRequest().build();
		}
	}
}