# Product manager Demo
Standard maven/spring boot configuration with some additions to make life easier. Standard mvn commands:
mvn clean install to build the project.
mvn spring-boot:run to run the project.


<B>All Endpoints information </B> http://localhost:8090/swagger-ui.html 


Run server locally  at http://localhost:8090/ + ENDPOINT example GET http://localhost:8090/v1/products

All external property configuration placed in application.properties file.
Percentage Discount amount is given in from application.properties
Quantitive Discount amount is given in from application.properties
You can create multiple discounts depending on type. Quantitive Discounts can have minimum bulk size to be applied or percent - given on discount creation of discount.
To get calculation of discounted product price - you have to:

1.Create product POST /v1/product
2.Create discount POST /v1/discount
3.Use endpoint to calculate price GET /v1/product/discount/price
(as provided in swagger http://localhost:8090/swagger-ui.html )

I tried to do this project as fast as possible and there was no business requirements to relate discounts with products - thats why there are no relations/mappings made between those two entities.

Test that may interest You which is relevant to the assignment context - DiscountServiceUnitTest




PS. If you have any questions or remarks please contact me at baltrukiewicz@gmail.com

Happy reviewing!

